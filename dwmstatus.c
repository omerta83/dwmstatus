#define _BSD_SOURCE
#define BATT_NOW        "/sys/class/power_supply/BAT0/charge_now"
#define BATT_FULL       "/sys/class/power_supply/BAT0/charge_full"
#define BATT_STATUS       "/sys/class/power_supply/BAT0/status"

#define VOL_ICON_HIGH   "\uE050"
#define VOL_ICON_MEDIUM "\uE04D"
#define VOL_ICON_LOW    "\uE04E"
#define VOL_ICON_MUTE   "\uE04F"

#define WIFI_ON         "\uE1BA"
#define WIFI_OFF        "\uE1DA"

#define BATTERY_UNKNOWN "\uE1A6"
/* #define BATTERY_CHARGING "\uE1A3" */
#define BATTERY_ALERT   "\uE19C"
#define BATTERY_STD     "\uE1A5"
#define BATTERY_EMPTY "\uF112"
#define BATTERY_CHARGING "\uF111"
#define BATTERY_FULL "\uF113"
#define BATTERY_HALF "\uF114"
#define BATTERY_LOW "\uF115"

#define SEPARATOR_ICON  "\uE5D4"

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <strings.h>
#include <sys/time.h>
#include <time.h>
#include <sys/types.h>
#include <sys/wait.h>

#include <alsa/asoundlib.h>
#include <alsa/control.h>

#include <X11/Xlib.h>

char *tzargentina = "America/Buenos_Aires";
char *tzutc = "UTC";
char *tzberlin = "Europe/Berlin";

static Display *dpy;

char *
smprintf(char *fmt, ...)
{
	va_list fmtargs;
	char *ret;
	int len;

	va_start(fmtargs, fmt);
	len = vsnprintf(NULL, 0, fmt, fmtargs);
	va_end(fmtargs);

	ret = malloc(++len);
	if (ret == NULL) {
		perror("malloc");
		exit(1);
	}

	va_start(fmtargs, fmt);
	vsnprintf(ret, len, fmt, fmtargs);
	va_end(fmtargs);

	return ret;
}

char *
getWifi(void) {
    /* char *cmd = "iwgetid --scheme"; */
    char *cmd = "iwconfig $(iwgetid | cut -d \" \" -f 1) | grep ESSID | awk -F: '{gsub(/\"/,\"\", $2);print $2}'";
    char *wifi = WIFI_OFF;
    /* char *status = malloc(0); */
    char *scheme = malloc(sizeof(char)*12);

    FILE *fp = NULL;
    /* FILE *fp = fopen("/sys/class/net/wlp4s0/operstate", "r"); */
    /* fscanf(fp, "%s", status); */
    /* if (fp == NULL || strcmp(status, "down") == 0) { */
    /*     fclose(fp); */
    /*     return wifi; */
    /* } */
    if ((fp = popen(cmd, "r")) == NULL) {
        pclose(fp);
        return wifi;
    }

    fscanf(fp, "%s", scheme);
    pclose(fp);

    /* if (scheme && scheme[0]) { */
    if (scheme && strstr(scheme, "off") == NULL) {
        wifi = WIFI_ON;
        /* return smprintf("%s %s", wifi, scheme); */
    }

    return smprintf("%s", wifi);
}

char *
getBat() {
    char *status = malloc(0);
    char *percent = malloc(0);
    char *cmd = "acpi -b | cut -d , -f 1,2 | cut -d : -f 2 | tr -d ','";

    FILE *fp = NULL;
    char *icon = BATTERY_EMPTY;
    if ((fp = popen(cmd, "r"))) {
        fscanf(fp, "%s %s", status, percent);
        pclose(fp);
        if (strcmp(status, "Charging") == 0 || strcmp(status, "Full") == 0 || strcmp(status, "Unknown") == 0) {
            icon = BATTERY_CHARGING;
        } else if (strcmp(status, "Discharging") == 0) {
        /* } else { */
            /* icon = BATTERY_STD; */
            char *end;
            long p = strtol(percent, &end, 10);
            if (p >= 90) {
                icon = BATTERY_FULL;
            } else if (p < 90 && p >= 50) {
                icon = BATTERY_HALF;
            } else {
                icon = BATTERY_LOW;
            }
        } else {
            percent = percent ? percent : "";
        }
        return smprintf("%s %s", icon, percent);
    }
    return smprintf("%s", icon);
}

char *
getbattery(){
    long lnum1, lnum2 = 0;
    long percent = 0;
    char *status = malloc(sizeof(char)*12);
    /* char s = '?'; */
    char *icon = BATTERY_UNKNOWN;
    FILE *fp = NULL;
    if ((fp = fopen(BATT_NOW, "r"))) {
        fscanf(fp, "%ld\n", &lnum1);
        fclose(fp);
        fp = fopen(BATT_FULL, "r");
        fscanf(fp, "%ld\n", &lnum2);
        fclose(fp);
        fp = fopen(BATT_STATUS, "r");
        fscanf(fp, "%s\n", status);
        fclose(fp);
        percent = lnum1/(lnum2/100);
        if (strcmp(status,"Charging") == 0 || strcmp(status, "Full") == 0) {
            icon = BATTERY_CHARGING;
        }
        else if (strcmp(status,"Discharging") == 0) {
            if (percent > 30)
                icon = BATTERY_STD;
            else
                icon = BATTERY_ALERT;
            /* s = '-'; */
            /* if (percent >= 90) */
            /*     icon = BATTERY_FULL; */
            /* else if (percent >= 75 && percent < 90) */
            /*     icon = BATTERY_75; */
            /* else if (percent >= 50 && percent < 75) */
            /*     icon = BATTERY_50; */
            /* else if (percent >= 25 && percent < 50) */
            /*     icon = BATTERY_25; */
            /* else */
            /*     icon = BATTERY_ALERT; */
        }
        /* else if (strcmp(status,"Full") == 0) { */
        /*     icon = BATTERY_FULL; */
        /*     #<{(| s = '='; |)}># */
        /* } */
        /* return smprintf("%c%ld%%", s,(lnum1/(lnum2/100))); */
        return smprintf("%s%ld%%", icon, percent);
    }
    else return smprintf("%s", icon);
}

int
getVol(void)
{
    long min, max, volume = 0;
    snd_mixer_t *handle;
    snd_mixer_selem_id_t *sid;
    const char *card = "default";
    const char *selem_name = "Master";

    snd_mixer_open(&handle, 0);
    snd_mixer_attach(handle, card);
    snd_mixer_selem_register(handle, NULL, NULL);
    snd_mixer_load(handle);

    snd_mixer_selem_id_alloca(&sid);
    snd_mixer_selem_id_set_index(sid, 0);
    snd_mixer_selem_id_set_name(sid, selem_name);
    snd_mixer_elem_t* elem = snd_mixer_find_selem(handle, sid);

    snd_mixer_selem_get_playback_volume_range(elem, &min, &max);
    snd_mixer_selem_get_playback_volume(elem, 0, &volume);
    snd_mixer_close(handle);

    return ((double)volume / max) * 100;
}

/* char* */
/* getTemp() { */
/*     char *cmd = "curl -s wttr.in | head -4 | tail -1 | sed 's,\x1B\[[0-9;]*[a-zA-Z],,g' | rev | sed -e 's/^[ \t]|)}>#/' | cut -d ' ' -f 1,2 | rev | tr -d ' '"; */
/* } */

void
settz(char *tzname)
{
	setenv("TZ", tzname, 1);
}

char *
mktimes(char *fmt, char *tzname)
{
	char buf[129];
	time_t tim;
	struct tm *timtm;

	memset(buf, 0, sizeof(buf));
	settz(tzname);
	tim = time(NULL);
	timtm = localtime(&tim);
	if (timtm == NULL) {
		perror("localtime");
		exit(1);
	}

	if (!strftime(buf, sizeof(buf)-1, fmt, timtm)) {
		fprintf(stderr, "strftime == 0\n");
		exit(1);
	}

	return smprintf("%s", buf);
}

void
setstatus(char *str)
{
	XStoreName(dpy, DefaultRootWindow(dpy), str);
	XSync(dpy, False);
}

char *
loadavg(void)
{
	double avgs[3];

	if (getloadavg(avgs, 3) < 0) {
		perror("getloadavg");
		exit(1);
	}

	return smprintf("%.2f %.2f %.2f", avgs[0], avgs[1], avgs[2]);
}

int
main(void)
{
	char *status;
        char *battery;
        char *vol_icon;
        char *wifi;
        char *separator = SEPARATOR_ICON;
        int vol = 0;

	if (!(dpy = XOpenDisplay(NULL))) {
		fprintf(stderr, "dwmstatus: cannot open display.\n");
		return 1;
	}

	for (;;sleep(10)) {
        /* while(!sleep(20)) { */
                /* battery = getbattery(); */
                battery = getBat();
                vol = getVol();
                wifi = getWifi();
                if (vol > 50) {
                    vol_icon = VOL_ICON_HIGH;
                } else if (vol <= 50 && vol > 0) {
                    vol_icon = VOL_ICON_MEDIUM;
                } else {
                    vol_icon = VOL_ICON_LOW;
                }

		status = smprintf(" %s%s%s %d%%%s%s ",
				battery, separator, vol_icon, vol, separator, wifi);
		setstatus(status);
                free(battery);
		free(status);
	}
        /* free(battery); */
        /* free(status); */

	XCloseDisplay(dpy);

	return 0;
}

